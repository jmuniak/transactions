import DS from 'ember-data';

export default DS.Model.extend({
    amount: DS.attr(),
    // attachements: [
    //     {
    //         url: "https://fakeimg.pl/350x200/?text=Hello"
    //     }
    // ],
    counterparty_name: DS.attr(),
    created_at: DS.attr(),
    credit: DS.attr(),
    currency: DS.attr(),
    debit: DS.attr(),
    id: DS.attr(),
    operation_type: DS.attr()
});
