import Ember from 'ember';

export default Ember.Component.extend({
    willRender() {
    $.getJSON('http://private-5d708-interviewfront.apiary-mock.com/transactions').then(data => {
      this.set('transactions', data.transactions);
    });
  }
});
